# CSS常用命名規則

## 區塊
- header 頭
- content 內容
- footer 尾
- main 主體
- sidebar 側欄
- banner 橫幅/廣告
***
## 導覽選單
- nav 導覽列
- subnav 子導覽列
- topnav 頂導覽列
- menu 菜單
- submenu 子菜單
- tab 標籤頁
- bread-crumb 麵包屑
***
## table
- column 欄目
***
## 列表
- list
- item
***
## 其他
- copyright 版權
- joinus 加入
- partner 合作夥伴
- logo 標誌
- status 狀態
- scroll 滾動
- search 搜尋
- msg 提示信息
- tips 小技巧
- gulid 指南
- service服務
- hot熱點
- download下載
- regsiter註冊
- loginbar登陸條
- btn按鈕
- vote投票
- note註釋
- friend-link 友情連結
- current當前
- shop購物車
- icon圖標
- wrapper/container 控制布局寬度
***
## 文本
- title 標題
