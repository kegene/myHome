`JS` `隨手記`
[TOC]

# Number 類型

## Number.EPSTLON

javascript特殊數字常數，大約2.22e-16。
使用此常數測試數字是否接近。

```javascript
let n = 0.1 + 0.2; // 0.30000000000000004
Math.abs(n - 0.3) < Number.EPSILON; // true;
n = 0.7 + 0.1; // 0.7999999999999999
Math.abs(n - 0.8) < Number.EPSILON // true;

```
