`JS` `隨手記`
[TOC]

# prototype

每個 function 都會有 prototype，利用繼承，節省重複 code

```javascript
function DemoA() {
  this.carNameFormA = 'BMW';
}
DemoA.prototype.callCarNameFormA = function() {
  return this.carNameFormA;
};
function DemoB() {
  this.carNameFormB = '四個圈圈';
}
DemoB.prototype = new DemoA();
DemoB.prototype.callCarNameFormB = function() {
  return this.carNameFormB;
};
var c = new DemoB();
console.log(c.callCarNameFormA()); // BMW
console.log(c.callCarNameFormB()); // 四個圈圈
```
