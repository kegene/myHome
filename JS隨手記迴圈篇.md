`JS` `隨手記`

[TOC]

# for in

## 單獨使用

能夠訪問到對象屬性時，返回true。
> 包含原型、實例
> 而hasOwnProperty()方法僅訪問實例

```javascript
function Person() {};

Person.prototype.name = 'k';

var personA = new Person();

console.log('name' in personA); // true;
console.log(personA.hasOwnProperty('name')); // false

personA.name = 'e';
console.log('name' in personA); // true;
console.log(personA.hasOwnProperty('name')); // true
```

利用in與hasOwnProperty檢查對象是原型還實例

```javascript
function hasPrototypeProperty(object, name){
  return !object.hasOwnProperty(name) && (name in object);
}
```

## 循環使用

適用所有可以枚舉的對象
> 即為數據屬性且Enumerable為true的對象

Object.keys()取代for-in

```javascript
function Person() {};

Person.prototype.name = 'k';

var personA = new Person();
var keys = Object.keys(Person.prototype);
console.log(keys); //"name"
var p1 = new Person();
p1.name = "e";
p1.age = 20;
var p1keys = Object.keys(p1);
console.log(p1keys); //"name,age"
```
