# vue-cli 隨手記

## vue-cli ?

- 快速建立專案(一次建立各種套件與資料夾)
- 專案默認使用 webpack，vue-cil 有一套默認設定(要自己設定也可以)
- 開發流程遵照一套方式
  > vue-cli 安裝流程直接參閱官網流程，在此跳過。

## 建立專案

### "介面模式"建立專案

1. `vue ui`

![Imgur](https://i.imgur.com/yeJEnRY.png)

:exclamation:運行環境預設為 localhost，若需要更改 IP，可以改成`vue ui --host [IP]`

2. vue ui 跑完後自動跳出管理介面網站

創建 > 建立新資料夾/新專案

導入 > 加入現有專案。
> 此範例選擇[創建]

![Imgur](https://i.imgur.com/S6QzurY.png)

1. 設定專案名字與套件，可選擇默認套件，或是手動設置，自行挑選安裝套件，如 vuex、router 等

![Imgur](https://i.imgur.com/FfYFoFK.jpg)

2. 範例使用手動設置，選了 router、vuex、babel、eslint，安裝完後，回頭看 IDE，新增了一堆檔案，長這樣:point_down:


![Imgur](https://i.imgur.com/RbVOCSw.jpg)
安裝的套件 router、vuxe 的 js 檔案都也自動新增了。

#### 新增更多插件

![Imgur](https://i.imgur.com/lN08Y4w.png)

### "指令模式"建立專案

`移動=上下鍵` `選擇=空白鍵` `確認=Enter`

1. creat 初始化專案，指令`vue create [name]`

1.1 選擇預設裝置(babel、eslint)，還是手動設置?

![Imgur](https://i.imgur.com/zBt24uM.png)

1.2 上一題選手動，這邊會問你那要裝哪些插件?![Imgur](https://i.imgur.com/Rm6lz5h.png)

1.3 Vue Router 的 mode(模式)使用 history 嗎? (另一個是 hash，會在 URL 加上#符號) ![Imgur](https://i.imgur.com/jTQWSX2.png)

1.4 ESLint 的嚴謹程度

![Imgur](https://i.imgur.com/SLDhD7d.png)

1.5 選擇代碼檢查時機 (這邊是多選哦)

![Imgur](https://i.imgur.com/PGNPuIX.png)

1.6 這些專案設定文件要存在哪裡?

![Imgur](https://i.imgur.com/iRsxUP3.png)

1.7 這些專案設定文件要加入(1.1 題)選擇裝置的項目內嗎?

![Imgur](https://i.imgur.com/KF6ZASm.png)

1.8 你的包裝管理要用哪個?

![Imgur](https://i.imgur.com/91CApXl.png)

2. 完成以上後開始建立專案

![Imgur](https://i.imgur.com/Cuxghl8.png)

3. 專案建立完成後，開啟專案`npm run serve`

3.1 run 甚麼東西，可以在 package.json 內的 scripts 設置。

`左邊key=命名，右邊value=動作`![Imgur](https://i.imgur.com/YrhlrFS.png)

4. 專案開發完成後，使用 npm run build，將 vue 編譯成 JS 與 HTML，扔到伺服器就可以看到畫面了。

#### 新增更多插件

> 報錯...哭哭，問題待解
> _Mon, May 13, 2019 4:10 PM_

![Imgur](https://i.imgur.com/yTTjlzw.png)

### vue.config.js

可選的設定檔，用來設定 vue-cli

> 要手動建立這支 js，如果這支存在，@vue/cli-service 會自動加載，並以這支檔案的設定值為主。
> [官方文檔++](https://cli.vuejs.org/zh/config/#%E5%85%A8%E5%B1%80-cli-%E9%85%8D%E7%BD%AE)
