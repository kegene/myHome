`JS` `隨手記` `Math`
[TOC]

# max、min

回傳 最大值、最小值

```javascript
var max = Math.max(2, 4, 6, 8, 10);
console.log(max); // 10;

var min = Math.min(2, 4, 6, 8, 10);
console.log(min); // 2;
```

- 取陣列的最大值、最小值

```javascript
var array = [2, 4, 6, 8, 10];
var maxUndefined = Math.max(array);
console.log(maxUndefined); // undefined;
// so
var arrayMax = Math.max.apply(Math, array);
console.log(arrayMax); // 10
// 最小值同理
```

# ceil()

無條件進位

```javascript
console.log(Math.ceil(1.4)); // 2
console.log(Math.ceil(1.5)); // 2
```

# floor()

無條件捨去

```javascript
console.log(Math.floor(1.4)); // 1
console.log(Math.floor(1.5)); // 1
```

# round()

四捨五入

```javascript
console.log(Math.round(1.4)); // 1
console.log(Math.round(1.5)); // 2
```

# random()

回傳 0~1 的隨機數

常見用法:point_down:
**Math.floor(Math.random() \* (總數-至少值) + 至少值 )**

```javascript
// 取1~10隨機數
const rand = Math.floor(Math.random() * 10 + 1);
// 取2~10隨機數
const rand = Math.floor(Math.random() * 9 + 2);
```

**function 包裝**

```javascript
function randoms(min, max) {
  const rand = max - min + 1;
  return Math.floor(Math.randmo() * rand + min);
}
let num = randoms(5, 10);

// 配合陣列

let colors = ["red", "green", "blue", "yellow", "black", "purple", "brown"];
let color = colors[randoms(0, colors.length - 1)];
console.log(color);
```

