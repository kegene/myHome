Document
===

[TOC]

# 網頁請求

## document.URL

取得網址

## document.domain

取得域名，且可以更動。

> 域名loose、tight不可以混著改，一下loose、一下tight，不然會報錯。

### 更動域名

不可以更改為不同域。

```javascript
console.log(document.URL); // abc.dfg.com
document.domain = 'abc.com';
document.domain = 'abc.io'; // Error
```

## document.referrer

取得來源頁面的URL

# 特殊集合

## document.forms

所有```<form>```

## document.images

所有```<img>```

## document.anchors

所有帶有name的```<a>```

## document.links

所有帶有href的```<a>```

# document.createElement(元素)

創建元素

```javascript
let div = document.createElement('div');
div.id = "item1";
div.className = "box";

// 增加元素節點
document.body.appendChild(div); // 加在最後面
// 或是這樣增加
document.body.insertBefore(div); // 加在最前面
document.body.insertBefore(div, null); // 加在最後面
// 或是這樣增加
document.body.replaceChild(div);
```

補充說明:
appendChild(對象) - 增加元素節點
insertBefore(對象, 排在n的前面) - 增加元素節點
replaceChild(newChild, 被取代的oldChild) - 增加元素節點

# document.createAttribute(屬性)

創建屬性

```html
<div id="box">I'm Box</div>

<script>
  let div = document.getElementById('box');
  let attr = document.createAttribute('is-attr-name');
  attr.value = '123456';
  div.setAttributeNode(attr);
  console.log(div); // <div id="box" is-attr-name="123456">I'm Box</div>
</script>
```

補充說明:
setAttributeNode - 增加屬性節點


# document.createTextNode(text)

創建文本節點

```javascript
let div = document.createElement('div');
let textNode = document.createTextNode('HI');
div.appendChild(textNode);
document.body.appendChild(div);
```

# document.normalize()

合併文本節點。

```javascript
let div = document.createElement('div');
let textNodeA = document.createTextNode('HI');
let textNodeB = document.createTextNode(':)');

div.appendChild(textNodeA);
div.appendChild(textNodeB);

console.dir(div.childNodes.length); // 2

div.normalize();

console.dir(div.childNodes.length); // 1
console.log(div.firstChild.nodeValue); // HI:)

document.body.appendChild(div);
```

# document.splitText(index)

分割文本節點

```javascript
let div = document.createElement('div');
let textNode = document.createTextNode('HI! :)');

div.appendChild(textNode);

console.dir(div.childNodes.length); // 1
console.log(div.firstChild.nodeValue); // HI! :)

let nextText = div.firstChild.splitText(3);

console.dir(div.childNodes.length); // 2
console.log(div.firstChild.nodeValue); // "HI!"
console.log(nextText); // " :)"

document.body.appendChild(div);
```

# DocumentFragment類型

避免瀏覽器反覆渲染DOM，可先將DOM對象存入DocumentFragment，在一次性的appendChild。

```javascript
let fragment = document.createDocumentFragment();
let ul = document.getElementById("myList");
let li = null;
for (let i=0; i < 3; i++){
  li = document.createElement("li");
  li.appendChild(document.createTextNode("Item " + (i+1)));
  fragment.appendChild(li);
}
ul.appendChild(fragment);
```


# Element

- nodeType = 1
- nodeName = 標籤名字 ; 或用tegName獲取標籤名字
- nodeValue = null
- parentNode = Document || Element
- 子節點 = Element || Text || Comment || ProcessingInstruction || CDATASection || EntityReference;

> 判斷標籤名字時，建議先轉為大寫或小寫，再來判斷。
> 小寫toLowerCase()
> 大寫toUpperCase()

## 方法

### element.getAttribute();

取得元素的attr

```HTMl
<div class="box" id="item1" style="color:red; font-size: 3em"></div>
```

```javascript
const div = document.getElementById('item1');
console.log(div.getAttribute('id')); // item1
console.log(div.getAttribute('class')); // box
console.log(div.getAttribute('style')); // color:red; font-size: 3em
```

or

```javascript
const div = document.getElementById('item1');
console.log(div.id); // item1
console.log(div.class); // box
console.log(div.getAttribute('style')); // CSSStyleDeclaration 對象
```

### element.setAttribute('屬性', '值')

存元素的attr

```HTMl
<div class="box" id="item1" style="color:red; font-size: 3em"></div>
```

```javascript
const div = document.getElementById('item1');
div.setAttribute("id", "item2");
div.setAttribute("abc", "value");
console.log(div.id); // item2
console.log(div.abc); // value
div.num = 666; // 這樣set自定義是無效的
console.log(div.num); // null
```

### element.removeAttribute('屬性')

刪除屬性。

*搭配上一個setAttribute()HTML範例，繼續。*

```javascript
div.removeAttribute('class');
```

## 屬性

### element.attributes

返回屬性列表NamedNodeMap (類似NodeList)

- getNamedItem(name) 返回nodeName === name的節點
- removeNamedItem(name) 移除nodeName === name的節點
- setNamedItem(node) 以節點的nodeName為索引添加節點
- item(pos) 返回位置為pos的節點

```HTML
<div id="test" class></div>
```

```javascript
let element = document.getElementById('test');
console.log(element.attributes); // { id, class };
console.log(element.attributes.getNamedItem("id").nodeValue); // "test";
console.log(element.attributes['id']); // "test";
console.log(element.attributes.id); // "test";

// 設定值
element.attributes["id"].nodeValue = "A";

// 但是設第二次，id改變，但HTML沒有更動。
// 取陣列後可更動
element.attributes[0].nodeValue = "B";

// 刪除
let oldAttr = element.attributes.removeNamedItem("id");

console.log(oldAttr); // id="B" node對象;
console.dir(oldAttr); // { id }

// 存
element.attributes.setNamedItem(oldAttr);

```