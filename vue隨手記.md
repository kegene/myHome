# vue 隨手記

## :class

當參數變動時，也會更新渲染。

### 用對象v-bind:class

**template模板**

```HTML
<div class="box" :class="{ open: isOpen, 'active': isActive }">Hello</div>
```

**vue資料**

```javascript
data() {
    return {
        isOpen: true,
        isActive: false
    }
}
```

或是 使用物件

```HTML
<div class="box" :class="classObj">Hello</div>
```

```javascript
data() {
    return {
        classObj: {
            open: true,
            active: false
        },
    }
}
```


或是 使用function，更彈性。

```HTML
<div class="box" :class="classObj">Hello</div>
```

```javascript
data() {
    return {
        open: true,
        active: false
    }
},
computed: {
  classObject() {
    return {
      isOpen: this.isOpen,
      'isActive': this.isActive
    }
  }
}
```

**以上html渲染結果**

```HTML
<div class="box open">Hello</div>
```

### 用陣列v-bind:class

**template模板**

```HTML
<div class="box" :class="[isOpen, isActive]">Hello</div>
```

**vue資料**

```javascript
data() {
    return {
        isOpen: 'open',
        isActive: 'active'
    }
}
```

**以上html渲染結果**

```HTML
<div class="box open active">Hello</div>
```

也可以使用三元運算

```HTML
<div class="box" :class="[isOpen ? isOpen : '', 'active'">Hello</div>
```

乾脆使用

```HTML
<div class="box" :class="[{open: isOpen}, 'active'">Hello</div>
```

## :style

### 對象v-bind:style

```HTML
<div class="box" :style="{color: colorhandler}">Hello</div>
```

```javascript
data() {
    return {
        isOpen: 'open',
        isActive: 'active'
    }
}
```

### 陣列v-bind:style

```HTML
<div class="box" :style="[{open: isOpen}, 'active'">Hello</div>
```


## @click="click(item)"

點擊切換 true, false

```
@click="click(item)"{
    item.selected = !item.selected
    console.log(item.selected)
}
```

## router

### router.push(location, onComplete?, onAbort?)

`來自vue官網說明`

| 聲明式                    | 編程式             |
| ------------------------- | ------------------ |
| `<router-link :to="...">` | `router.push(...)` |

```js
// 字符串
router.push('home');

// 对象
router.push({ path: 'home' });
router.push({ name: 'home' });

// 命名的路由
router.push({ name: 'user', params: { userId: '123' } });

// 带查询参数，变成 /register?plan=private
router.push({ path: 'register', query: { plan: 'private' } });
```

如果寫了 path，那 params 無效，需使用 query 傳值

```javascript
const userId = '123';
router.push({ name: 'user', params: { userId } }); // -> /user/123
router.push({ path: `/user/${userId}` }); // -> /user/123
// 这里的 params 不生效
router.push({ path: '/user', params: { userId } }); // -> /user
```

### params、query 傳參數

```vue
// 平常見到的router設置 import Login from '@/views/account/LoginByPassword.vue'; export default [
// 設置當路由未填寫，為"/"時，顯示login.vue。
{
    path: '/', component: Login,
},
{
    path:'/login',
    name: 'login',
    component: Login,
    meta: { layout: 'account-login',},
},
{
    // 設置params傳參數
    // code為變數，使用params傳得參數會將值帶入。
    // query不需設置這些，直接用即可。
    path: '/inviter/:code', name: 'inviter',
    component: LoginEmail,
    meta: { layout: 'account-login',},
    children: [
        {
            path: 'children',
            component: () =>import('@/views/children.vue'), name: 'children'
            }
    ],
},
{
    path: '/account/login-phone',
    name: 'login-password',
    component: () => import('@/views/loginPhone.vue'),
},
];
```

_:exclamation: 刷新頁面之後，params 傳得參數不會被保留，而 query 仍會保留，因為 query 是看 url。_

#### params、query 傳參數 - 如何使用在 vue 中

```typescript & vue
<template>
    <button @click="changRouter"></button>
    <router-link :to="{ name: 'router1', params: { id: status, id2: status3 }, query: { queryId: status2 } }">
    </router-link>
    <div class="params">{{ this.$route.params }}</div>
    <div class="query">{{ this.$route.query }}</div>
</template>

<script>
import { Component, Vue } from 'vue-property-decorator';

export default class Home extends Vue {
    changRouter() {
        this.$router.push(
            {
                name:'router1',
                params: {
                    id: status,
                    id2: status3,
                },
                query: {
                    queryId: status2,
                },
            }
        );
    }
}
</script lang="ts">
```
