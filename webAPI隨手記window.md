`web api` `隨手記`

# window 底下有許多 function 可以使用

> 這些 function 使用時可以直接打函式名字加上()即可執行。

[TOC]

# window.alert() 彈出訊息框

# window.close() 關閉瀏覽器視窗

# window.confirm() 談出確認取消框

# window.createPopup() 建立彈框

只有在 IE 才會出現的彈窗
![Imgur](https://i.imgur.com/nApzU8k.png)
(圖片來源為菜鳥教程)

# window.focus() 取得焦點

# window.blur() 移除視窗焦點

# window.moveBy() 相對位置移動視窗

# window.moveTo() 移動視窗到指定位置

# window.open() 開新瀏覽器視窗

# window.print() 輸出目前窗口內容

# window.setInterval() 計時器

# window.setTimeout() 計時器

# window.clearInterval() 取消 setInterval()設定的計時器

# window.clearTimeout() 取消 setTimeout()設定的計時器

# window.resizeBy() 調整視窗大小

# window.resizeTo() 調整視窗大小

# window.scrollBy() 滾動內容

# window.scrollTo() 滾動內容

# window.request​Animation​Frame(callback) 監聽動畫事件

優化以往使用計時器操控動畫時的耗能與效果。
> 大部分使用計時器操作畫面，時間是給死的(無考量設備與效能等因素)，這樣執行上可能造成動畫流程錯誤。

[webDevelopers說明](https://developers.google.com/web/fundamentals/performance/rendering/optimize-javascript-execution?hl=zh-tw)

- 針對視覺更新避免 setTimeout 或 setInterval；一律改為使用 requestAnimationFrame。
- 將長時間執行的 JavaScript 從主執行緒移動至 Web Worker。
- 使用微任務，以讓 DOM 在多個畫面內變更。
- 使用 Chrome DevTools 的 Timeline 和 JavaScript Profiler，以評估 JavaScript 的影響。

## 範例: 使用request​Animation​Frame操作動畫 停與動。

參考來源 [blog](https://blog.camel2243.com/2017/01/31/javascript-requestanimationframe-%E5%84%AA%E5%8C%96%E5%8B%95%E7%95%AB%E6%95%88%E7%8E%87%E8%88%87%E8%B3%87%E6%BA%90/) & [MDN](https://developer.mozilla.org/zh-TW/docs/Web/API/Window.requestAnimationFrame#Syntax) & js30-day2

```HTML
<div id="wrap">
  <div id="box"></div>
</div>
```

```css
#wrap {
  width: 400px;
  height: 50px;
  position: relative;
  background: #000;
}
#box {
  width: 50px;
  height: 50px;
  position: absolute;
  background-color: #fff;
}
```

```javascript
var el = document.getElementById("box");
var left = 0;
var bound = false;
var swich = true;

var requestID = requestAnimationFrame(moveBox);

function moveBox() {
  if (!swich) return cancelAnimationFrame(requestID);
  if(left === 0 || left === 350) {
    bound = !bound;
  }
  (bound) ? left++ : left--;
  el.style.left = left + 'px';
  requestID = window.requestAnimationFrame(moveBox);
}

el.addEventListener('click', function(){
  swich = !swich;
  requestID = window.requestAnimationFrame(moveBox);
});
```