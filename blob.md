# Blob

## 介紹

Blob（Binary Large Object）表示二進制類型的大對象。在數據庫管理系統中，將二進制數據存儲為一個單一個體的集合。Blob 通常是影像、聲音或多媒體文件。

前端使用常見於傳文件給後端，以 content-type: multipart/form-data 格式傳送，前端將blob對象存到formData，再傳給後端。
一般是input type=file直接將blob檔案傳給後端，有時候需要把特地資料轉成blob，比如圖片透過canvas修改後，需要再轉檔，或是第三方套件處理完會是以base64返回，這時候就要轉檔。


### base64 轉 blob

#### fetch
注意：Fetch不支持IE
```javascript
var url = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg=="

fetch(url)
.then(res => res.blob())
.then(blob => {
  var fd = new FormData()
  fd.append('image', blob, 'filename')
  
  console.log(blob)

  // Upload
  // fetch('upload', {method: 'POST', body: fd})
})
```

網路上有看過new File, new Blob轉檔版本，由於File不是每個裝置都支援，所以傾向於使用Blob，另外File本身繼承Blob來的。

#### new Blob
```javascript
/**
 * 轉Blob
 * @param { string } dataURI
 * @return {Blob}
 */
function DataURIToBlob(dataURI) {
    const splitDataURI = dataURI.split(',');
    const byteString = splitDataURI[0].indexOf('base64') >= 0 ?
        atob(splitDataURI[1]) :
        decodeURI(splitDataURI[1]);
    const mimeString = splitDataURI[0].split(':')[1].split(';')[0];

    const ia = new Uint8Array(byteString.length);
    for (let i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], {type: mimeString});
}
```


### 讀取Blob
```javascript
var reader = new FileReader();
reader.addEventListener("loadend", function() {
   // reader.result contains the contents of blob as a typed array
});
reader.readAsArrayBuffer(blob);
```