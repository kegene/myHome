`Canvas` `隨手記`
[TOC]

# How to do ?

## HTML 建立畫板範圍

```HTML
<canvas id="draw" width="800" height="800"></canvas>
```

## 其他動作都由 Script 完成

起手式 = 找到畫板目標，下指令取得 CanvasRenderingContext2D。

```javascript
const canvas = document.querySelector('#draw');
const ctx = canvas.getContext('2d');
```

# Canvas key

> 圖文參考 MDN

| 中文                             | key                                                                                                            | 備註                                                                                    |
| -------------------------------- | -------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------- |
| 線條寬度                         | lineWidth                                                                                                      |                                                                                         |
| 線條結尾樣式                     | lineCap                                                                                                        | butt、round、square ![Imgur](https://i.imgur.com/ditjVzk.png)                           |
| 線條和線條間接合處的樣式         | lineJoin                                                                                                       | round、bevel、miter ![Imgur](https://i.imgur.com/gJpQPo2.png)                           |
| 限制當兩條線相交時交接處最大長度 | miterLimit                                                                                                     | 限制 lineJoin 交接處的長度。==看不太懂，待解==                                          |
| 線條顏色                         | strokeStyle                                                                                                    | 支援單詞、16 進位、rgb、rgba                                                            |
| 填滿顏色                         | fillStyle                                                                                                      | 支援單詞、16 進位、rgb、rgba                                                            |
| 填充矩形                         | fillRect(x 座標, y 座標, width, height)                                                                        |                                                                                         |
| 建立新起點                       | beginPath()                                                                                                    |                                                                                         |
| 終點                             | stroke()                                                                                                       | 一個 beginPath()搭一個 stroke()                                                         |
| 圓                               | arc(x 座標, y 座標, radius 半徑, startAngle 圓的起始, endAngle 圓的結束, anticlockwise 順時逆時(可選 boolean)) |                                                                                         |
| 透明度                           | globalAlpha                                                                                                    |                                                                                         |
| 顏色重疊樣式                     | globalCompositeOperation                                                                                       | [MDN](https://developer.mozilla.org/zh-CN/docs/Web/API/Canvas_API/Tutorial/Compositing) |
