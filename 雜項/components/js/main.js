window.onload = function(){
    const header = document.getElementById('header');
    const footer = document.getElementById('footer');

    const url = [
      axios.get("./components/header.html"),
      axios.get("./components/footer.html"),
    ]

    // axios.all([...url]).then(function(res) {
    //   header.innerHTML = res[0].data;
    //   footer.innerHTML = res[1].data;
    // })
    // .catch(function(error) {
    //   console.log(error);
    // });

    axios.all([...url]).then(axios.spread(function(apiA, apiB) {
      header.innerHTML = apiA.data;
      footer.innerHTML = apiB.data;
    }))
    .catch(function(error) {
      console.log(error);
    });
}