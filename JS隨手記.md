`JS` `隨手記`
[TOC]

# 數組 array 迭代函式

## some(item, index, array)

返回 boolean。
全部都 false，返回 false，其中有 true，返回 true

```javascript
let array = [-1, -2, 1, 2];
const domeA = array.some(person => person > 0);
const domeB = array.some(person => {
  return person > 0;
});

console.log(domeA); // true
console.log(domeB); // true
```

## every(item, index, array)

返回 boolean。
全部都 true，返回 true，其中有 false，返回 false

```javascript
let array = [-1, -2, 1, 2];
const domeA = array.every(person => person > 0);
const domeB = array.every((person, index, array) => {
  return person > 0;
});

console.log(domeA); // false
console.log(domeB); // false
```

## find(item, index, array)

返回 第一個為 true 的值

```javascript
let array = [-1, -2, 1, 2];
const domeA = array.find(person => person > 0);
const domeB = array.find(person => {
  return person > 0;
});

console.log(domeA); // 1
console.log(domeB); // 1
```

## findIndex(item, index, array)

返回 索引值
如果 array[1]為 true，返回 1，因為在索引值[1]找到的
如果 array[4]值為 true，返回 4，因為在索引值[4]找到的

```javascript
let array = [-1, -2, 0, 22, 55];
const domeA = array.findIndex(person => person > 0);
const domeB = array.findIndex(person => {
  return person > 0;
});

console.log(domeA); // 3
console.log(domeB); // 3
```

## filter(item, index, array)

返回 新陣列 (為 true 的值 組成新陣列)

```javascript
let array = [-1, -2, 1, 2];
const domeA = array.filter(item => item > 0);
const domeB = array.filter((item, index, array) => {
  console.log(item); // 每項的內容
  console.log(index); // 索引值
  console.log(array); // [-1, -2, 1, 2]
  return item > 0;
});

console.log(domeA); // [1, 2]
console.log(domeB); // [1, 2]
```

## forEach(item, index, array)

返回 ... 恩..也可不返回，可替代 for 迴圈使用。

```javascript
var nums = [1, 2, 3, 4, 5, 4, 3, 2, 1];
nums.forEach(function(item, index, array) {
  console.log(item); // 每項的內容
  console.log(index); // 索引值
  console.log(array); // [1,2,3,4,5,4,3,2,1];
});
```

## map(item, index, array)

返回 新陣列 (任何內容的值組成新陣列，主要用在處理資料後返回新陣列)

```javascript
let array = ['-1', '-2', '1', '2'];
const domeA = array.map(item => item * 1);
const domeB = array.map((item, index, array) => {
  console.log(item); // 每項的內容
  console.log(index); // 索引值
  console.log(array); // [-1, -2, 1, 2]
  return item * 1;
});

console.log(domeA); // [-1, -2, 1, 2];
console.log(domeB); // [-1, -2, 1, 2];
```

# 數組 array 函式

## slice(start, end)

返回新陣列

```javascript
const array = ['a', 'b', 'c', 'd', 'e'];
const newArrayA = array.slice(0, 2);
const newArrayB = [...array.slice(0, 2), ...array.slice(2, 5)];

console.log(newArrayA); // ['a', 'b']
console.log(newArrayB); // ['a', 'b', 'c', 'd', 'e'];
```

## reduce((prev, cur, index, array), 起始值)

每進行一次迭代，會將 prev 值加起來，書例與 js30 範例如下:

> reduceRight()從陣列最右邊開始迭代

數字範例 Code

```javascript
var values = [1, 2, 3, 4, 5];
var sum = values.reduce(function(prev, cur, index, array) {
  console.log(prev);
  console.log(cur);
  console.log(array);
  return prev + cur;
}, 0); // 初始值設數字，且為0，不寫的話，預設是從陣列第一項開始。
console.log(sum); //15
```

物件範例 Code

```javascript
const data = [
  'car',
  'car',
  'truck',
  'truck',
  'bike',
  'walk',
  'car',
  'van',
  'bike',
  'walk',
  'car',
  'van',
  'car',
  'truck'
];
const transportation = data.reduce(function(obj, item) {
  if (!obj[item]) {
    obj[item] = 0;
  }
  obj[item]++;
  return obj;
}, {});
```

# function

## apply、call、bind

- apply(作用域, 一包參數)
- call(作用域, 幾個參數放幾個(用逗點隔開))
- bind(作用域)

作者範例 code

```javascript
window.color = 'red';
var o = { color: 'blue' };
function sayColor() {
  alert(this.color);
}
sayColor(); //red
sayColor.call(this); //red
sayColor.call(window); //red
sayColor.call(o); //blue

var objectSayColor = sayColor.bind(o);
objectSayColor(); //blue
```

# 處理數字

## toFixed(point)

指定該數字要到小數點第幾位，範圍 0~20 個小數位。

```javascript
var numA = 25;
console.log(numA.toFixed(2)); // 25.00

var numB = 25.005;
console.log(numB.toFixed(2); // 25.01 自動四捨五入
```

## toExponential(point)

指數表示法(e 表示法)，參數一樣代表要到小數第幾位

```javascript
var num = 10;
console.log(num.toExponential(1)); // 1.0e+1
```

## toPrecision(位數)

返回 可能是 fixed 格式、exponential 格式，該方法會自行判斷用哪種回傳

```javascript
var num = 99;
alert(num.toPrecision(1)); //"1e+2"
alert(num.toPrecision(2)); //"99"
aler t(num.toPrecision(3)); //"99.0"
```
