`JS` `隨手記`
[TOC]

# String 類型

構造函數創建 string

```javascript
var stringObject = new String('hello');
```

字面創建 string

```javascript
var string = 'hello';
```

## 每個 sring 類型都有 length 屬性

```javascript
var string = 'hello';
console.log(string.length); // 6
```

## 字符方法

### 字符轉換大小寫

```javascript
var string = 'hello world';

// 針對不同地區環境實行
console.log(string.toLocaleUpperCase()); // 'HELLO WORLD'
console.log(string.toLocaleLowerCase()); // 'hello world'

console.log(string.toUpperCase()); // 'HELLO WORLD'
console.log(string.toLowerCase()); // 'hello world'
```

> 作者建議使用 toLocaleLowerCase()和 toLocaleUpperCase()，因為不知道自己的代碼會在哪個語言環境下運行。

### charAt(位置)

返回 對應位置的值

```javascript
var string = 'hello';
console.log(string.charAt(1)); // 'e'
```

這樣寫其實也可以，不過 IE8 以下會得到 undefined

```javascript
var string = 'hello';
console.log(string[1]); // 'e'
```

### charCodeAt(位置)

返回 對應位置的值的字符編碼
效果跟fromCharCode()相反。
```javascript
var string = 'hello';
console.log(string.charCodeAt(1)); // 101
```

## 字符串操作方法

### concat()

返回 新字符串，由一個或多個字符串組成。

```javascript
var string = 'hello ';
var stringConcatA = string.concat('world');
var stringConcatB = string.concat('world ', '😃', '!!!');

console.log(string); // 'hello '
console.log(stringConcatA); // 'hello world'
console.log(stringConcatB); // 'hello world 😃!!!'
```

> 或是使用+將字串相連就好。

### slice(起始位置, end)

```javascript
var string = '0123456789';
console.log(string.slice(2)); // '23456789'
console.log(string.slice(-2)); // '89'
console.log(string.slice(2, 5)); // '234'

// 第一個參數為負數
console.log(string.slice(-2, 5)); // ''
console.log(string.slice(-5, 2)); // ''
console.log(string.slice(-2, -5)); // ''

// 第二個參數為負數
console.log(string.slice(2, -3)); // '23456'
console.log(string.slice(5, -2)); // '567'
console.log(string.slice(-5, -2)); // '567'
```

圖解:point_down::point_down:
|    正著數     |    0    |   1    |   2    |   3    |   4    |   5    |   6    |   7    |   8    |   9    |
| :-----------: | :-----: | :----: | :----: | :----: | :----: | :----: | :----: | :----: | :----: | :----: |
|  **倒著數**   | **-10** | **-9** | **-8** | **-7** | **-6** | **-5** | **-4** | **-3** | **-2** | **-1** |
|   slice(2)    |    0    |   1    | ==2==  | ==3==  | ==4==  | ==5==  | ==6==  | ==7==  | ==8==  | ==9==  |
|   slice(-2)   |    0    |   1    |   2    |   3    |   4    |   5    |   6    |   7    | ==8==  | ==9==  |
|  slice(2, 5)  |    0    |   1    | ==2==  | ==3==  | ==4==  |   5    |   6    |   7    |   8    |   9    |
| slice(2, -3)  |    0    |   1    | ==2==  | ==3==  | ==4==  | ==5==  | ==6==  |   7    |   8    |   9    |
| slice(5, -2)  |    0    |   1    |   2    |   3    |   4    | ==5==  | ==6==  | ==7==  |   8    |   9    |
| slice(-5, -2) |    0    |   1    |   2    |   3    |   4    | ==5==  | ==6==  | ==7==  |   8    |   9    |

> 以正著數來說，從 8 數到 2 往前數，會壞掉，屢試不爽。
> 以倒著數來說，同樣道理，-3 數到-8，一樣壞掉。

針對負數 圖解:point_down::point_down:
|    正著數     |   0   |   1   |   2   |   3   |   4   |   5   |   6   |   7   |   8   |   9   |
| :-----------: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|   slice(-2)   |   0   |   1   |   2   |   3   |   4   |   5   |   6   |   7   | ==8== | ==9== |
| slice(2, -3)  |   0   |   1   | ==2== | ==3== | ==4== | ==5== | ==6== |   7   |   8   |   9   |
| slice(5, -2)  |   0   |   1   |   2   |   3   |   4   | ==5== | ==6== | ==7== |   8   |   9   |
| slice(-5, -2) |   0   |   1   |   2   |   3   |   4   | ==5== | ==6== | ==7== |   8   |   9   |

> substr(-2) 可以當作 substr(8)，因為-2+10 = 8
> substr(2, -3) 可以當作 substr(2, 7)，因為-3+10 = 7
> substr(5, -2) 可以當作 substr(5, 8)，因為-2+10 = 8
> substr(-5, -2) 可以當作 substr(5, 8)，因為-5+10 = 5；-2+10 = 8

### substr(起始位置, Length)

> 用 length 形容怪怪的，我覺得第二個參數比較像從起始點數來第幾位
> ex: (2,5) 從第二個開始，取五個值。

```javascript
var string = '0123456789';
console.log(string.substr(2)); // '23456789'
console.log(string.substr(-2)); // '89'

console.log(string.substr(2, 5)); // '23456'
console.log(string.substr(-2, 5)); // '89'
console.log(string.substr(-5, 3)); // '567'

// 第二個參數為負數 (等同於 0)
console.log(string.substr(5, -2)); // ''
console.log(string.substr(2, -5)); // ''
console.log(string.substr(-2, -5)); // ''
console.log(string.substr(-5, -2)); // ''
```

> substr 將第二個負數當作 0 ，所以取 0 個值。

圖解:point_down::point_down:
|    正著數     |    0    |   1    |   2    |   3    |   4    |   5    |   6    |   7    |   8    |   9    |
| :-----------: | :-----: | :----: | :----: | :----: | :----: | :----: | :----: | :----: | :----: | :----: |
|  **倒著數**   | **-10** | **-9** | **-8** | **-7** | **-6** | **-5** | **-4** | **-3** | **-2** | **-1** |
|   substr(2)   |    0    |   1    | ==2==  | ==3==  | ==4==  | ==5==  | ==6==  | ==7==  | ==8==  | ==9==  |
|  substr(-2)   |    0    |   1    |   2    |   3    |   4    |   5    |   6    |   7    | ==8==  | ==9==  |
| substr(2, 5)  |    0    |   1    | ==2==  | ==3==  | ==4==  | ==5==  | ==6==  |   7    |   8    |   9    |
| substr(-5, 3) |    0    |   1    |   2    |   3    |   4    | ==5==  | ==6==  | ==7==  |   8    |   9    |
針對負數 圖解:point_down::point_down:
|    正著數     |   0   |   1   |   2   |   3   |   4   |   5   |   6   |   7   |   8   |   9   |
| :-----------: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|  substr(-2)   |   0   |   1   |   2   |   3   |   4   |   5   |   6   |   7   | ==8== | ==9== |
| substr(-5, 3) |   0   |   1   |   2   |   3   |   4   | ==5== | ==6== | ==7== |   8   |   9   |

> substr(-2) 可以當作 substr(8)，因為-2+10 = 8
> substr(-5, 3) 可以當作 substr(5, 3)，因為-5+10 = 5

### substring(起始位置, end)

```javascript
var string = '0123456789';
console.log(string.substring(2)); // '23456789'
console.log(string.substring(-2)); // '0123456789'
console.log(string.substring(2, 5)); // '234'

// 第一個參數為負值，所以是(0, end)
console.log(string.substring(-2, 5)); // '01234'
console.log(string.substring(-5, 2)); // '01'

// 第二個參數為負值，所以是(起始值, 0);
console.log(string.substring(2, -5)); // '01'
console.log(string.substring(5, -2)); // '01234'

// 兩個都為負值，所以是(0,0);
console.log(string.substring(-2, -5)); // ''
console.log(string.substring(-5, -2)); // ''
```

圖解:point_down::point_down:
|      正著數      |    0    |   1    |   2    |   3    |   4    |   5    |   6    |   7    |   8    |   9    |
| :--------------: | :-----: | :----: | :----: | :----: | :----: | :----: | :----: | :----: | :----: | :----: |
|    **倒著數**    | **-10** | **-9** | **-8** | **-7** | **-6** | **-5** | **-4** | **-3** | **-2** | **-1** |
|   substring(2)   |    0    |   1    | ==2==  | ==3==  | ==4==  | ==5==  | ==6==  | ==7==  | ==8==  | ==9==  |
|  substring(-2)   |  ==0==  | ==1==  | ==2==  | ==3==  | ==4==  | ==5==  | ==6==  | ==7==  | ==8==  | ==9==  |
| substring(2, 5)  |    0    |   1    | ==2==  | ==3==  | ==4==  |   5    |   6    |   7    |   8    |   9    |
| substring(-5, 2) |  ==0==  | ==1==  |   2    |   3    |   4    |   5    |   6    |   7    |   8    |   9    |
| substring(5, -2) |  ==0==  | ==1==  | ==2==  | ==3==  | ==4==  |   5    |   6    |   7    |   8    |   9    |

> substring 會把所有負值當作 0

針對負數 圖解:point_down::point_down:
|      正著數      |   0   |   1   |   2   |   3   |   4   |   5   |   6   |   7   |   8   |   9   |
| :--------------: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
|  substring(-2)   | ==0== | ==1== | ==2== | ==3== | ==4== | ==5== | ==6== | ==7== | ==8== | ==9== |
| substring(-5, 2) | ==0== | ==1== |   2   |   3   |   4   |   5   |   6   |   7   |   8   |   9   |
| substring(5, -2) | ==0== | ==1== | ==2== | ==3== | ==4== |   5   |   6   |   7   |   8   |   9   |

> substr(-2) 可以當作 substr(0)
> substr(-5, 2) 可以當作 substr(0, 2)
> substr(5, -2) 可以當作 substr(5, 0)

### localeCompare()

返回-1、0、1，比較兩個字串符，依照字母表的先後順序來比對；

```javascript
var string = 'b';

console.log(string.localeCompare('a')); // 1
console.log(string.localeCompare('b')); // 0
console.log(string.localeCompare('c')); // -1
```

> localeCompare()依地區決定如何實行，以美國英國來說，localeCompare()會區分大小寫，比較規則也不一樣，大寫排在小寫前面。

### fromCharCode()

將字符編碼轉為字串符，所以返回 字串符
效果跟charCodeAt()相反。
```javascript
console.log(String.fromCharCode(104,101,108,108,111)); // 'hello';
```

## 字符串位置方法

### indexOf('要找的字符', 從索引值哪裡開始)

由左至右查詢

```javascript
var string = '012345543210';
var demoA = string.indexOf('2');
var demoB = string.indexOf('2', 5);
console.log(`demoA: ${demoA},\nstring[${demoA}]: ${string[demoA]}`);
console.log(`demoB: ${demoB},\nstring[${demoB}]: ${string[demoB]}`);
```

### lastIndexOf('要找的字符', 從索引值哪裡開始)

由右至左查詢

```javascript
var string = '012345543210';
var demoA = string.lastIndexOf('2');
var demoB = string.lastIndexOf('2', 5);
console.log(`demoA: ${demoA},\nstring[${demoA}]: ${string[demoA]}`);
console.log(`demoB: ${demoB},\nstring[${demoB}]: ${string[demoB]}`);
```

## 匹配字符串

### match(正規式)

返回 陣列，符合正規的對象組成陣列對象。

```javascript
var txt = 'abc';
var rule = /abc/;

var ruleMatch = txt.match(rule);
console.log(ruleMatch);
console.log(ruleMatch[0]);
console.log(ruleMatch.index);
console.log(ruleMatch.input);
```

### exec(被檢查的值)

返回 陣列，符合正規的對象組成陣列對象。
> 返回得跟match一樣

```javascript
var txt = 'abc';
var rule = /abc/;
var ruleExec = rule.exec(txt);
console.log(ruleExec);
console.log(ruleExec[0]);
console.log(ruleExec.index);
console.log(ruleExec.input);
```

### search(正規式)

返回 數字

```javascript
var txt = 'abc';
var rule = /abc/;
var ruleSearchA = txt.search(rule);
var ruleSearchB = 'ab'.search(rule);
var ruleSearchC = 'efabcd'.search(rule);

console.log(ruleSearchA); // 0 在[0]找到了
console.log(ruleSearchB); // -1 沒找到
console.log(ruleSearchC); // 2 在[2]找到了
```

### replace(RegExp / 字串, 字串 / 函數) `用來檢查內容同時替換內容的方法`

返回 被替換完之後的字串

```javascript
var txt = "a,b,c,e,f,g";

// 使用字串符檢查: 符合條件的第一個進行替換，然後結束。
var resA = txt.replace('a','A');
console.log(resA); // 'A,b,c,e,f,g'

/// 使用正規檢查: 符合條件的"都"替換，然後結束。
var resB = txt.replace(/[a-z]/gi, 'oh');
console.log(resB); // 'oh,oh,oh,oh,oh,oh'

// 第二個參數給function
var resC = txt.replace(/[a-z]/gi, function(match, pos, originalText) {
  console.log('match==',match);
  console.log('pos==',pos);
  console.log('originalText==',originalText);
  if (match) {
    return match + ' is match\n';
  }
})
console.log(resC);
```

### split()

返回 陣列

```javascript
var txt = 'a,b,c,d,e,f';

var arrA = txt.split(',');
console.log(arrA); // ["a", "b", "c", "d", "e", "f"]
var arrB = txt.split(',', 2 );
console.log(arrB); // ["a", "b"]
var arrC = txt.split(/[a-z]/);
console.log(arrC); // ["", ",", ",", ",", ",", ",", ""]
```

## 去除字串空格 trim()

返回 去掉前後空格的新字串。

```javascript
var string = '  hello haha   ';
var newString = string.trim();
console.log(newString); // 'hello haha'
```
