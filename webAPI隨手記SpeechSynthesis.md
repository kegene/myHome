Speech​Synthesis
`這是一個還在實驗的功能(MDN)`
===

語音服務的控制接口。
> 當add語音，依照先後順序播放，因為很像排隊的概念，所以我稱為播放隊伍。

參考來源 js30-day23 & [MDN](https://developer.mozilla.org/zh-CN/docs/Web/API/SpeechSynthesis)

屬性

- SpeechSynthesis.paused
  > 目前是暫停狀態嗎? 返回boolean
- SpeechSynthesis.pending
  > 目前播放隊伍還有等待播放的語音嗎? 返回boolean
- SpeechSynthesis.speaking
  > 目前是播放狀態嗎? 返回boolean

事件

- SpeechSynthesis.onvoiceschanged
  - 當方法SpeechSynthesis.getVoices()返回的SpeechSynthesisVoice改變時觸發。

方法

- SpeechSynthesis.cancel() 移除播放隊伍所有等候播放的語音。
- SpeechSynthesis.getVoices() 取得當前可撥放的語系
- SpeechSynthesis.pause() 暫停語音播放
- SpeechSynthesis.resume() toggle暫停播放
- SpeechSynthesis.speak() add語音進入播放隊伍

## 簡易範例

或是參考js30-day23

```HTML
<button id="btn">播放語音</button>
```

```javascript
let el = document.getElementById("btn");
let msg = new SpeechSynthesisUtterance(); // new一個空的語系對象
let synth = window.speechSynthesis;
let voices = [];
let lang = ['zh-TW'];

function getVoice() {
  voices = synth.getVoices(); // 當前支援語系列表
  voices = voices.filter((item)=> lang.includes(item.lang) ); // 找到想要的語系
}
function btnHandler() {
  msg.text = '哈囉'; // 存 播放內容
  msg.voice = voices[0]; // 存 語系
  taggier(); // 播放
}
function taggier(isSpeak = true) {
  speechSynthesis.cancel();
    if (isSpeak) {
    speechSynthesis.speak(msg);
  }
}

speechSynthesis.addEventListener('voiceschanged', getVoice);
el.addEventListener('click', btnHandler);
```